x = 15
y = 10
z = 5

if (x >= y) and (x >= z):
    largest = x
elif (x >= y) and (z >= x):
    largest = z
else:
    largest = y
print("The largest number is: ", largest)
