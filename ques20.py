N = int(input('Enter the number of elements to be printed : '))

a = 0
b = 1
c = 0

print('Generated Series : ', end='')

for i in range(N):
    print(a, end=', ')

    c = a + b

    a = b
    b = c

print()
Max = int(input('Enter a Max Number : '))

print('Generated Series : ', end='')

a = 0
b = 1
c = 0

while a <= Max:
    print(a, end=', ')

    c = a + b

    a = b
    b = c
