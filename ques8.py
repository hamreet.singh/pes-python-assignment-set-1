tup = (1, 3, 5, 2, 6, 4, 7, 6, 9, 3)  # Tuples are immutable and hashable (used as key)

print(tup)

newTup = tup[2:8]
print(newTup)

print(tup * 3)

print(tup + newTup)
