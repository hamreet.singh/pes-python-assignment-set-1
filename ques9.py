def addComplex(a1, a2):
    return a1 + a2


a1 = complex(5, 3)
a2 = complex(1, 7)
print('Addition gives:', addComplex(a1, a2))


def subComplex(a1, a2):
    return a1 - a2


a1 = complex(5, 3)
a2 = complex(1, 7)
print('Subtraction gives:', subComplex(a1, a2))


def mulComplex(a1, a2):
    return a1 * a2


a1 = complex(5, 3)
a2 = complex(1, 7)
print('Multiplication gives:', mulComplex(a1, a2))


def divComplex(a1, a2):
    return a1 / a2


a1 = complex(5, 3)
a2 = complex(1, 7)
print('Division gives:', divComplex(a1, a2))
